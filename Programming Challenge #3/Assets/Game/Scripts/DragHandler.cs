﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject Player;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private NinjaController _ninjaController;
    private float faceDirection = 0.1f;

    private void Start()
    {
        _rigidbody2D = Player.GetComponent<Rigidbody2D>();
        _animator = Player.GetComponent<Animator>();
        _ninjaController = Player.GetComponent<NinjaController>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Vector3 dragVectorDirection = (eventData.position - eventData.pressPosition).normalized;
        _animator.SetFloat("inputY", 0.0f);
        _animator.SetFloat("inputX", 0.0f);
        GetDragDirection(dragVectorDirection);

        _animator.SetTrigger("Attack");
        _rigidbody2D.velocity = new Vector2();
        _ninjaController.attack = true;
    }

    private void GetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);

        

        if (positiveX > positiveY)
        {
            if (dragVector.x > 0)
            {
                _animator.SetFloat("inputX", faceDirection);

            }
            else
            {
                _animator.SetFloat("inputX", faceDirection * -1);
            }
        }
        else
        {
            if (dragVector.y > 0)
            {
                _animator.SetFloat("inputY", faceDirection);

            }
            else
            {
                _animator.SetFloat("inputY", faceDirection * -1);
            }
        }
    }
}
