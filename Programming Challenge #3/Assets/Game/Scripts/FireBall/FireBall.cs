﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    public float lifeTime = 4.0f;

    void Update()
    {
        StatusCheck();
    }

    public void StatusCheck()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0.0f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
         Destroy(this.gameObject);
    }
}
