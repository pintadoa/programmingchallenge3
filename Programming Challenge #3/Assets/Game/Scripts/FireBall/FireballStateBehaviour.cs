﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballStateBehaviour : StateMachineBehaviour
{
    public float speedShot = 200.0f;

    private GameObject player;
    private GameObject fireball;
    private Rigidbody2D fireBallRB;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<NinjaController>().gameObject;
        fireball = animator.GetComponent<NinjaController>().fireBall;

        GameObject cannonBallCopy = Instantiate(fireball, player.transform.position, player.transform.rotation) as GameObject;
        fireBallRB = cannonBallCopy.GetComponent<Rigidbody2D>();

        // Adds force in the desired direction
        
        if (animator.GetFloat("inputY") < 0)
        {
            fireBallRB.AddForce((-player.transform.up) * speedShot);
        }
        else if (animator.GetFloat("inputX") > 0)
        {
            fireBallRB.AddForce((player.transform.right) * speedShot);
        }
        else if (animator.GetFloat("inputX") < 0)
        {
            fireBallRB.AddForce((-player.transform.right) * speedShot);
        } else
        {
            fireBallRB.AddForce(player.transform.up * speedShot);
        }
    }
}
