﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    private float time;
    private float coins;

    public Text uiTime;
    public Text uiCoins;

    // Start is called before the first frame update
    void Start()
    {
        time = 0.0f;
        coins = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        uiTime.text = time.ToString();
    }
}
