﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class NinjaController : MonoBehaviour, IAnimationCompleted
{
    public float speed = 5.0f;
    public float movementThreshold = 0.5f;
    public KeyCode attackKey;
    public GameObject fireBall;

    public GameObject[] touchControllers;

	private Vector2 inputDirection;
	private Rigidbody2D _rigidbody2D;
	private Animator _animator;
    [HideInInspector]
    public bool attack = false;


    private Vector2[] MoveDirections = new Vector2[] {   new Vector2(-1, 0),
                                                    new Vector2(1, 0),
                                                    new Vector2(0, 1),
                                                    new Vector2(0, -1) };

    private void Start()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();
	}

    public void AnimationCompleted(int shortHashName)
    {
        attack = false;
    }

    public void OnTouchMove(int inputTouch)
    {
        inputDirection = MoveDirections[inputTouch];
        _rigidbody2D.velocity = new Vector2(inputDirection.x * speed, inputDirection.y * speed);

        // Set the input values on the animator
        _animator.SetFloat("inputX", inputDirection.x);
        _animator.SetFloat("inputY", inputDirection.y);
        _animator.SetBool("isWalking", true);
    }

    public void OnTouchRelease()
    {
        _rigidbody2D.velocity = new Vector2();
        _animator.SetBool("isWalking", false);
    }

    // Update is called once per frame
    void Update()
    {
      
	}
}
