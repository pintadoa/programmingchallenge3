﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public float Maxhealth = 100;
    private float health;
    public Transform respawnLocation;

    // Start is called before the first frame update
    void Start()
    {
        health = Maxhealth;
        Respawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Respawn();
        }
    }

    public void Respawn()
    {
        health = Maxhealth;
        this.gameObject.transform.position = respawnLocation.position;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
    }

}
