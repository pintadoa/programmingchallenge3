﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrapActiveTrigger : MonoBehaviour
{
    public GameObject player;
    public float damage = 100.0f;

    private PlayerManager playerManager;

    private void Start()
    {
        playerManager = player.GetComponent<PlayerManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Animator>();
        playerManager.TakeDamage(damage);
    }
}
