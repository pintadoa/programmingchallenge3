﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : MonoBehaviour
{
    public float resetTime = 10.0f;
    public string trigger;

    private bool inRange;
    private float timer;
    private Animator animator;
    private AudioSource warningAudio;

    private void Start()
    {
        inRange = false;
        timer = resetTime;
        warningAudio = GetComponent<AudioSource>();
        animator = this.gameObject.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (inRange)
        {
            if (StopWatch())
            {
                animator.SetTrigger(trigger);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<NinjaController>())
        {
            inRange = true;
            warningAudio.Play(0);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<NinjaController>())
        {
            inRange = false;
            warningAudio.Pause();
        }
    }

    private bool StopWatch()
    {
        timer -= Time.deltaTime;
        if (timer <= 0.0f)
        {
            timer = resetTime;
            return true;
        }
        return false;
    }
}
