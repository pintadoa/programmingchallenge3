﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyRandomController : MonoBehaviour
{
    public MenuClassifier Menu1;
    public MenuClassifier Menu2;

    public void ShowMenu1()
    {
        MenuManager.Instance.showMenu(Menu1);
        MenuManager.Instance.hideMenu(Menu2);
    }

    public void ShowMenu2()
    {
        MenuManager.Instance.showMenu(Menu2);
        MenuManager.Instance.hideMenu(Menu1);
    }
}
